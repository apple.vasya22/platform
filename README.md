# Hipster Shop platform

Данный проект предназначен для разворачивания в gcloud


# Подготовка системы
Установка google-cloud-sdk 

    brew install --cask google-cloud-sdk
Инициализация gcloud  

    gcloud init  
Подключение к своему пространству в gcloud

	gcloud auth application-default login

## Клонирование и настройка проекта

Клонирование проекта

	git clone https://gitlab.com/apple.vasya22/platform.git'
Для дальнейшей настройки необходимо узнать название своего проекта в gcloud:

	gcloud config get-value project
В файл terraform.tfvars вносим данные о названии проекта и предпочитаемом регионе (пример - us-central1) по аналогии с terraform.tfvars-clear

	project_id =  "<your project id>"
	region =  "<choose region>"
	
## Разворачивание проекта с помощью terraform
Инициализация terraform

	terraform init 
Применение манифеста

	terraform apply --auto-approve

## Проверка

Получим внешний ip адрес сервиса

	kubectl get service frontend-external | awk '{print $4}'